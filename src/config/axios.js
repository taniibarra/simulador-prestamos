import axios from "axios";

const clienteAxios = axios.create({
  baseURL: "https://wired-torus-98413.firebaseio.com/",
});

export default clienteAxios;
