import {
  AGREGAR_SOLICITUD,
  AGREGAR_SOLICITUD_EXITO,
  AGREGAR_SOLICITUD_ERROR,
  DESCARGAR_SOLICITUD,
  DESCARGAR_SOLICITUD_EXITO,
  DESCARGAR_SOLICITUD_ERROR,
  ELIMINAR_SOLICITUD,
  ELIMINAR_SOLICITUD_EXITO,
  ELIMINAR_SOLICITUD_ERROR,
} from "../types/index";
import clienteAxios from "../config/axios";
import Swal from "sweetalert2";

export function solicitarPrestamo(solicitud) {
  return async (dispatch) => {
    dispatch(agregarSolicitud());

    try {
       await clienteAxios.post("/users.json", solicitud);
      
      dispatch(agregarSolicitudExito(solicitud));
    } catch (error) {
      console.log(error);
      dispatch(agregarSolicitudError(true));

      Swal.fire({
        icon: "error",
        title: "Hubo un error",
        text: "Hubo un error, intenta de nuevo",
      });
    }
  };
}

const agregarSolicitud = () => ({
  type: AGREGAR_SOLICITUD,
  payload: true,
});

const agregarSolicitudExito = (solicitud) => ({
  type: AGREGAR_SOLICITUD_EXITO,
  payload: solicitud,
});

const agregarSolicitudError = (estado) => ({
  type: AGREGAR_SOLICITUD_ERROR,
  payload: estado,
});

export function obtenerSolicitudAction() {
  return async (dispatch) => {
    dispatch(obtenerSolicitud());

    try {
      const respuesta = await clienteAxios.get("/users.json");
      //console.log(Object.values(respuesta.data));
      dispatch(descargaSolicitudExito(Object.values({}, respuesta.data)));
    } catch (error) {
      console.log(error);
      dispatch(descargaSolicitudError());
    }
  };
}

const obtenerSolicitud = () => ({
  type: DESCARGAR_SOLICITUD,
  payload: true,
});

const descargaSolicitudExito = (solicitudes) => ({
  type: DESCARGAR_SOLICITUD_EXITO,
  payload: solicitudes,
});

const descargaSolicitudError = () => ({
  type: DESCARGAR_SOLICITUD_ERROR,
  payload: true,
});

export function eliminarSolicitudAction(dni) {
  return async (dispatch) => {
    dispatch(eliminarSolicitud(dni));
    //console.log(dni);
    try {
      const resultado = await clienteAxios.delete("/users.json/", {
        params: { foo: `${dni}` },
      });
      console.log(resultado);
      dispatch(eliminarSolicitudExito());

      Swal.fire("Eliminada!", "Su solicitud ha sido eliminada.", "success");
    } catch (error) {
      console.log(error);
      dispatch(eliminarSolicitudError());
    }
  };
}

const eliminarSolicitud = (dni) => ({
  type: ELIMINAR_SOLICITUD,
  payload: dni,
});

const eliminarSolicitudExito = () => ({
  type: ELIMINAR_SOLICITUD_EXITO,
});

const eliminarSolicitudError = () => ({
  type: ELIMINAR_SOLICITUD_ERROR,
});
