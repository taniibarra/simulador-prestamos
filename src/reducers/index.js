import { combineReducers } from "redux";
import solicitudReducer from "./solicitudReducer";
//import scoreReducer from "./scoreReducer";

export default combineReducers({
  solicitudes: solicitudReducer,
  //score: scoreReducer,
});
