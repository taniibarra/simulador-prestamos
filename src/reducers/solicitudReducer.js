import {
  AGREGAR_SOLICITUD,
  AGREGAR_SOLICITUD_EXITO,
  AGREGAR_SOLICITUD_ERROR,
  DESCARGAR_SOLICITUD,
  DESCARGAR_SOLICITUD_EXITO,
  DESCARGAR_SOLICITUD_ERROR,
  ELIMINAR_SOLICITUD,
  ELIMINAR_SOLICITUD_EXITO,
  ELIMINAR_SOLICITUD_ERROR,
} from "../types/index";

const initialState = {
  solicitudes: [],
  error: null,
  loading: false,
  eliminarSolicitud: null,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case DESCARGAR_SOLICITUD:
    case AGREGAR_SOLICITUD:
      return {
        ...state,
        loading: action.payload,
      };
    case AGREGAR_SOLICITUD_EXITO:
      return {
        ...state,
        loading: false,
        solicitudes: [...state.solicitudes, action.payload],
      };
    case AGREGAR_SOLICITUD_ERROR:
    case DESCARGAR_SOLICITUD_ERROR:
    case ELIMINAR_SOLICITUD_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case DESCARGAR_SOLICITUD_EXITO:
      return {
        ...state,
        loading: false,
        error: null,
        solicitudes: action.payload,
      };
    case ELIMINAR_SOLICITUD:
      return {
        ...state,
        eliminarSolicitud: action.payload,
      };
    case ELIMINAR_SOLICITUD_EXITO:
      return {
        ...state,
        solicitudes: state.solicitudes.filter(
          (solicitud) => solicitud.dni !== state.eliminarSolicitud
        ),
        eliminarSolicitud: null,
      };
    default:
      return state;
  }
}
