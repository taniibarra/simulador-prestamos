import React from "react";
import Home from "./components/home/Home.components";
import Header from "./components/header/Header.components";
import Solicitud from "./components/solicitud/Solicitud";
import EditarSolicitud from "./components/solicitud/EditarSolicitud";

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import { Provider } from "react-redux";
import store from "./store";

function App() {
  return (
    <Router>
      <Provider store={store}>
        <Header />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/solicitud/nuevo" component={Solicitud} />
          <Route
            exact
            path="/solicitud/editar/:id"
            component={EditarSolicitud}
          />
        </Switch>
      </Provider>
    </Router>
  );
}

export default App;
