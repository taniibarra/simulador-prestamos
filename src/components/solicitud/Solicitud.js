import React, { useEffect, useState } from "react";
import Solicitudes from "./Solicitudes";
import axios from 'axios'

import { useSelector, useDispatch } from "react-redux";
import { obtenerSolicitudAction } from "../../actions/solicitudActions";
//import { solicitarScore } from "../../actions/scoreAction";

const Solicitud = () => {
  const [status, setStatus] = useState("");


 const axiosConfig = axios.create({
    baseURL: "https://api.moni.com.ar/",
    headers: {
      credential: "ZGpzOTAzaWZuc2Zpb25kZnNubm5u",
    },
  });
  
  const res = axiosConfig.get("/api/v4/scoring/pre-score/36220524")
  res.then((respuesta) =>  {
    console.log(respuesta)
    }) .catch((error) => {console.log(error)})
    

 /*const consultarAPI =  () => {
    let headers = new Headers({
      'credential': 'ZGpzOTAzaWZuc2Zpb25kZnNubm5u'
    })
    const api = fetch("https://api.moni.com.ar/api/v4/scoring/pre-score/36220524", {
      headers: headers,
      })
    api.then((respuesta) => {console.log(respuesta)})
    .catch((error) => {console.log(error)})
  }
  consultarAPI()*/

  const dispatch = useDispatch();

  useEffect(() => {
    const cargarSolicitud = () => dispatch(obtenerSolicitudAction());
    cargarSolicitud();
  }, []);

  /*useEffect(() => {
    const cargarEstado = () => dispatch(solicitarScore())
    cargarEstado()
  }, [])

  const validarScore = (score) => dispatch(solicitarScore(score));

 const getScore = (e) => {
    e.preventDefault()

    validarScore({
      status,
    });
  
  }*/


  //const datos = useSelector((state) => state.score.score);

  const solicitudes = useSelector((state) => state.solicitudes.solicitudes);
  //console.log(solicitudes);
  const error = useSelector((state) => state.solicitudes.error);
  const cargando = useSelector((state) => state.solicitudes.loading);

  return (
    <>
      {error ? (
        <p className="font-weight-bold alert alert-danger text-center mt-4">
          Hubo un error
        </p>
      ) : null}

      {cargando ? <p className="text-center">Cargando...</p> : null}

      <table className="table table-striped my-5">
        <thead className="bg-primary table-dark ">
          <tr>
            <th scope="col">Nombre</th>
            <th scope="col">Apellido</th>
            <th scope="col">Genero</th>
            <th scope="col">Email</th>
            <th scope="col">DNI</th>
            <th scope="col" >Estado</th>
          </tr>
        </thead>
        <tbody>
          {solicitudes === 0
            ? "no hay solicitudes"
            : Array.from(solicitudes).map((solicitud, id) => (
                <Solicitudes key={id} solicitud={solicitud} />
              ))}
        </tbody>
      </table>
    </>
  );
};

export default Solicitud;

/* {datos && datos.length === undefined
            ? "No hay score"
            : datos.map((score) => (
                <Solicitudes key={score.detail} score={score} />
              ))} onChange={(e) => setStatus(e.target.value)}  onSubmit={getScore}*/
