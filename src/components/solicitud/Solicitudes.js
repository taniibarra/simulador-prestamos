import React from "react";
import { Link } from "react-router-dom";

import { useDispatch } from "react-redux";
import { eliminarSolicitudAction } from "../../actions/solicitudActions";

import Swal from "sweetalert2";

const Solicitudes = ({ solicitud }) => {
  const { name, last, genre, email, dni, status } = solicitud;

  const dispatch = useDispatch();

  const confirmDelete = (dni) => {
    // preguntar al usuario
    Swal.fire({
      title: "¿Desea eliminar ésta solicitud?",
      text: "Recuerde que ésta accion no se puede revertir!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si, eliminar!",
    }).then((result) => {
      if (result.isConfirmed) {
        // pasar al action
        dispatch(eliminarSolicitudAction(dni));
      }
    });
  };

  return (
    <tr>
      <td>{name}</td>
      <td>{last}</td>
      <td>{genre}</td>
      <td>{email}</td>
      <td>{dni}</td>
      <td>
        <span className="font-weight-bold">{status}</span>
      </td>
      <td className="acciones">
        <Link to={`/solicitud/editar/${dni}`} className="btn btn-primary mr-2">
          Editar
        </Link>
        <button
          type="button"
          className="btn btn-danger"
          onClick={() => confirmDelete(dni)}
        >
          Eliminar
        </button>
      </td>
    </tr>
  );
};

export default Solicitudes;
