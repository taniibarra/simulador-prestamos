import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import {
  FormControl,
  Input,
  InputLabel,
  FormHelperText,
  Grid,
  MenuItem,
  Select,
} from "@material-ui/core";
import "./_home.scss";
import { solicitarPrestamo } from "../../actions/solicitudActions";

const options = ["Femenino", "Masculino"];

const Home = ({ history }) => {
  const [name, setName] = useState("");
  const [last, setLast] = useState("");
  const [genre, setGenre] = useState("");
  const [email, setEmail] = useState("");
  const [dni, setDni] = useState(0);

  const dispatch = useDispatch();

  const cargando = useSelector((state) => state.solicitudes.loading);
  const error = useSelector((state) => state.solicitudes.error);


  const agregarSolicitud = (solicitudes) =>
    dispatch(solicitarPrestamo(solicitudes));

  const clickSolicitud = (e) => {
    e.preventDefault();

    // validar form
    if (
      name.trim() === "" ||
      last.trim() === "" ||
      genre.trim() === "" ||
      email.trim() === "" ||
      dni <= 7
    ) {
      return;
    }
    // si no hay errores

    //crear nueva solicitud
    agregarSolicitud({
      name,
      last,
      genre,
      email,
      dni,
    });

    // redireccionar
    history.push("/solicitud/nuevo");
  };

  return (
    <>
      <div className="home-container">
        <div className="home-content">
          <div className="home-body">
            <form onSubmit={clickSolicitud}>
              <Grid item md={12}>
                <FormControl margin="dense">
                  <InputLabel>Nombre</InputLabel>
                  <Input
                    id="name"
                    type="text"
                    aria-describedby="name-helper"
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                  />
                  <FormHelperText id="name-helper">
                    Ingresa tu nombre
                  </FormHelperText>
                </FormControl>
              </Grid>
              <Grid item md={12}>
                <FormControl>
                  <InputLabel>Apellido</InputLabel>
                  <Input
                    id="last"
                    type="text"
                    aria-describedby="last-helper"
                    value={last}
                    onChange={(e) => setLast(e.target.value)}
                  />
                  <FormHelperText id="last-helper">
                    Ingresa tu apellido
                  </FormHelperText>
                </FormControl>
              </Grid>
              <FormControl
                style={{ width: 200, alignSelf: "center", marginBottom: 10 }}
              >
                <InputLabel id="genre">Genero</InputLabel>
                <Select
                  labelId="select"
                  id="select-genre"
                  value={genre}
                  onChange={(e) => setGenre(e.target.value)}
                  input={<Input />}
                >
                  {options.map((option) => (
                    <MenuItem key={option} value={option}>
                      {option}
                    </MenuItem>
                  ))}
                </Select>
                <FormHelperText id="id-helper">
                  Seleccion tu genero
                </FormHelperText>
              </FormControl>
              <Grid item md={12}>
                <FormControl>
                  <InputLabel>Email</InputLabel>
                  <Input
                    id="email"
                    type="email"
                    aria-describedby="email-helper"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                  />
                  <FormHelperText id="email-helper">
                    Ingresa tu Email
                  </FormHelperText>
                </FormControl>
              </Grid>
              <Grid item md={12}>
                <FormControl>
                  <InputLabel>Numero de DNI</InputLabel>
                  <Input
                    id="id"
                    type="number"
                    aria-describedby="dni-helper"
                    value={dni}
                    onChange={(e) => setDni(e.target.value)}
                  />
                  <FormHelperText id="id-helper">Ingresa tu DNI</FormHelperText>
                </FormControl>
              </Grid>
              <button type="submit" className="btn btn-danger">
                Solicitar
              </button>
              {cargando ? <p>Solicitando...</p> : null}
              {error ? <p>Hubo un error</p> : null}
            </form>
          </div>
        </div>
      </div>
    </>
  );
};

export default Home;
